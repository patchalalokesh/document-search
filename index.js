const express = require('express');
const app = express();
var cors=require('cors');
var bodyparser = require('body-parser');
var fileUploadroute = require('./routes/fileuploadDownload.route');
var filterroute = require('./routes/filter.route');
const multer = require('multer');
var port=process.env.PORT || 3000;
app.use(express.json());
app.use(cors());
app.use(bodyparser.urlencoded({extended:true}));
app.use(bodyparser.json());
app.use('/upload', fileUploadroute);
app.use('/filter', filterroute);
app.listen(port, () => {
    console.log('listening on *:3000');
  });