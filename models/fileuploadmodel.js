const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const documentSchema = new Schema({
    fileUrl: {
        type: String
    },
    format:
    {
      type: String,
      required:true,
      trim:true
    },
    filename:
    {
      type: String,
      required:true,
      trim:true 
    },
    size:
    {
      type:String,
      required:true,
      trim:true

    },
    createdDate:
    {
      type: Date,
      default: Date.now
    },
   lat: {
      type:Number,
    },
    lng:{
      type:Number,
    }
});
module.exports=mongoose.model("documentsearch", documentSchema);