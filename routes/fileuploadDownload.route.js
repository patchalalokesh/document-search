const express = require('express');
const multer = require('multer');
const mongoose = require('mongoose');
var fs = require('fs');
var formidable = require('formidable');

const pdf = require('pdf-parse');
var filereader = require('../services/parsingbodycontentof_file');
var amazonService = require("../services/amazons3bucketservice");
const elasticSearchService=require('../services/elasticsearchservice');
var XLSX = require('xlsx');
require('dotenv').config();
const fileuploadmodel = require('./../models/fileuploadmodel');
const fileuploaddownload=express.Router();
const db =  process.env.DB;
console.log(db);
mongoose.connect(db);
const upload = multer({
    limits: {
        fileSize: 25*1000000
    },
    fileFilter(req, file, cb) {
        if (!file.originalname.match(/\.(pdf|PDF|doc|DOC|docx|DOCX|txt|TXT|xlsx|csv|CSV)$/)) {
            req.fileValidationError="Please select a text file";
            return cb(new Error('Please select a text file'))
        }

        cb(undefined, true)
    }
})


fileuploaddownload.post('/upload',upload.single('file'),  async(req,res)=>{
    if(req.fileValidationError)
    {
        res.status(400).send({"error_message":req.fileValidationError})
    }
    if(req.file)
    {
        var filecontent='';
        var filepath='';
        console.log('inside',req.file);
        let filename=req.file.originalname;
        let {lat=0, lng=0}=req.body;
         console.log(lat,lng);

        var fileextension = filereader.getFileExtension(filename);
        console.log(fileextension);

    
        //for getting the content inside the file

        switch (fileextension) {
            case  '.PDF': case '.pdf':
                await pdf(req.file.buffer).then(function(data) {
 
                    // number of pages
                    // console.log(data.numpages);
                    // number of rendered pages
                    // console.log(data.numrender);
                    // PDF info
                    // console.log(data.info);
                    // PDF metadata
                    // console.log(data.metadata); 
                    // PDF.js version
                    // check https://mozilla.github.io/pdf.js/getting_started/
                    // console.log(data.version);
                    // PDF text
                    console.log(data.text); 
                    filecontent=data.text;
                        
                });
        break;
        case '.xlsx':case  '.xls':
            var result = {};
            let data = new Uint8Array(req.file.buffer);
            var workbook = XLSX.read(data, {
            type: 'array'
            });
            workbook.SheetNames.forEach(function (sheetName) {
            var roa = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName], {
            header: 1
            });
            if (roa.length) result[sheetName] = roa;
            });
            filecontent = JSON.stringify(result);
            break;
            case '.doc': case '.docx': case 'DOCX':
                console.log('inside docx');
            await filereader.extract(req.file.buffer).then(response=>{
                filecontent =response;
            }).catch(error=>{
                console.log(error);
            })
            break;
            
                case '.txt': case '.csv': case '.CSV':
               await new Promise((resolve,reject)=>{
                   resolve(req.file.buffer.toString());
               }).then(response=>{
                   filecontent=response;
               }).catch(error=>{
                   console.log(error);
               })
            break;

            default:
            filecontent = filename;
            }

            // console.log(filecontent, "125");
            try
            {
            await amazonService.uploadFile(req.file.buffer,req.file.originalname).then((response)=>{
            console.log(response, '115');
             console.log("inside");
            filepath=response.Location;
            }).catch(error=>{
            console.log(error);
            throw(error);
            });
             console.log(filepath);
            let upload=new fileuploadmodel({fileUrl:filepath, filename:req.file.originalname, format:fileextension, size:req.file.size,lat,lng})
            let uploadedResponse= await upload.save();
            console.log(uploadedResponse);
            // putting data into elastic search
            // id,body, filename, filesize, fileformat, createddate, lat,lng
            elasticSearchService.createDocument(uploadedResponse._id.toString(), filecontent, uploadedResponse.filename, uploadedResponse.size,uploadedResponse.format,uploadedResponse.createdDate,uploadedResponse.lat,uploadedResponse.lng).then(response=>{
                console.log(response);
                res.status(200).send(uploadedResponse);
            }).catch(error=>{
                console.log(error);
            })

        }
            catch(error)
            {
             console.log(error);
             res.status(500)
            }
    }})

fileuploaddownload.get('/downloaddoc', (req,res)=>{
     console.log(req.query._id, "entered");
    fileuploadmodel.findOne(
        {'_id': req.query._id}
    ).then(doc => {

        // console.log('type of doc.file: ' + typeof doc.file);
        // returns:
        // type of doc.file: object

        console.log(doc.file);
        // returns:
        // Binary {
        //   _bsontype: 'Binary',
        //   sub_type: 0,
        //   position: 1146504,
        //   buffer: <Buffer 50 4b 03 04 14 00 00 00 08 00 12 66 6f 4d f8 64 19 ... > }

        console.log(doc.format);
        res.writeHead(200,   {
            'Content-Type': doc.format,
            'Content-Disposition': 'attachment; filename="' + doc.filename + '"'
          });
        
        // This one doesn't work
        // res.end(doc.file, 'binary');
        // returns:
        // TypeError: First argument must be a string or Buffer

        res.end(new Buffer(doc.file, 'binary') );
    });
})

fileuploaddownload.get('/uploadbulkdata',(req,res)=>{
    elasticSearchService.uploadbulkdata().then((res)=>{
        console.log("sucess")
    }).catch(error=>{
        console.log(error);
    });
})

// fileuploaddownload.post('/creatBucket',(req,res)=>{
//     console.log(req.body.bucketname);
//     amazonService.createBucket(req.body.bucketname)
//     // .then(response=>{
//     //     console.log(response)
//     // }).catch(error=>{
//     //     console.log(error);
//     // });

// })

module.exports=fileuploaddownload;
