const express = require('express');
require('dotenv').config();
const filterroute=express.Router();
const mongoose = require('mongoose');
require('dotenv').config();
const fileuploadmodel = require('./../models/fileuploadmodel');
const getuploadfilterservice=require('../services/getuploadfilter');
const elasticSearchService = require('../services/elasticsearchservice');
const elasticFindAllService= require('../services/elasticsearchalldataretrival');
// let  demo = require('../services/demo');
const db =  process.env.DB;
mongoose.connect(db);
filterroute.get('/filter', async(req,res)=>{
    console.log(req.query.search);
    const { page = 1, limit = 10 } = req.query;
    if(req.query.search) {
        try
        {
            
           
            const regex = new RegExp(escapeRegex(req.query.search), 'gi');
            // Get all campgrounds from DB
            let count = await  fileuploadmodel.count({filename: regex});

            //console.log(count);

            let result = await fileuploadmodel.find({filename: regex}).select('-file').limit(limit * 1)
            .skip((page - 1) * limit)
            .exec();

            if(result.length < 1) {
                noMatch = "No campgrounds match that query, please try again.";
                return   res.status(200).send({"message":"No items found"})
            }
            return res.status(200).send({result,
                totalPages: Math.ceil(count / limit),
                currentPage: page});
        }

        catch
        {
           return res.status(500)
        }
    } 
    else
    {
        getuploadfilterservice.getuploadfiles(page, limit).then((response=>{
            //console.log(response);
            res.status(200).send(response)
        })).catch(error=>{
            res.status(400);
        });
    }
     return res.status(400);
    // console.log('working');
    // res.send("working");
})

filterroute.get('/getrecords', async(req,res)=>{
    
     const { page = 1, limit = 10 } = req.query;
     
     getuploadfilterservice.getuploadfiles(page,limit).then((response=>{
         //console.log(response);
         res.status(200).send(response)
     })).catch(error=>{
         res.status(400);
     });


})
//search function
function escapeRegex(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
};


//elastic search filter
filterroute.get('/filterrecordsel', (req,res)=>{
    
    let {searchTerm=''}=req.query;
    if(searchTerm)
    {
        elasticSearchService.searchForQuery(searchTerm).then((response)=>{
            console.log(response, "84");
            res.status(200).send(response);
        }).catch(error=>{
            console.log(error);
        });
    }
    else
    {
        res.status(400).send({"message":"Some fields are missing"})
    }

})

//filter based on location
filterroute.get('/lat_lon_search',(req,res)=>{
    let {lat,lon,distance}=req.query;
    console.log(lat,lon);
    if(((lat!='0')||(lon!='0'))&&distance)
    {
        if(req.query.countrycode)
        { console.log(req.query.countrycode);
            elasticSearchService.SearchusingcountryCode(req.query.countrycode).then(response=>{
                res.status(200).send(response)
            }).catch(error=>{
                res.status(500).send(error);
            });
          
        }
        else
        {
            console.log('inside else');
            elasticSearchService.locationSearch(lat,lon,distance).then(response=>{
                console.log(response);

                res.status(200).send(response);
            }).catch(error=>{
                console.log(error);
                res.status(500).send(error);
    
            });
        }
   
    }
    else
    {
        elasticFindAllService.getalldata().then((response)=>{
            response=elasticFindAllService.filter(response);
            console.log(response.length);
            // console.log('response',response);
            res.status(200).send(response);
        },error=>{
            // console.log('error',error);
            res.status(500).send(error);
        })
        // res.status(400).send({"message":"Some fields are missing"})
    }
})



module.exports=filterroute;
