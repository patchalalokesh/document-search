require('dotenv').config();
const { Client } = require('@elastic/elasticsearch');
const { filterType } = require('jimp');
const client = new Client({
    cloud: {
      id: process.env.ELASTIC_SEARCH_CLOUDID
    },
    auth: {
      username:process.env.ELASTIC_SEARCH_USERNAME,
      password: process.env.ELASTIC_SEARCH_PASSWORD
    }
  })


let getalldata= async ()=>{
    console.log('called');
    // const length=57421;
    const completeData = []
    const responseQueue = []

    // start things off by searching, setting a scroll timeout, and pushing
    // our first response into the queue to be processed
    const response = await client.search({
      index: 'latlngsearch',
      // keep the search results "scrollable" for 30 seconds
      scroll: '1m',
      // for the sake of this example, we will get only one result per search
      size: 10000,
      // filter the source to only include the quote field
      body: {
        query: {
          match_all: {}
        }
      }
    })
  
    responseQueue.push(response)
  
    while (responseQueue.length) {
      const { body } = responseQueue.shift()
  
      // collect the titles from this response
      body.hits.hits.forEach(function (hit) {
        completeData.push(hit)
      })
  
      // check to see if we have collected all of the quotes
      if (body.hits.total.value === completeData.length) {
        // console.log('Every quote', completeData);
       
        break;
      }
  
      // get the next response if there are more quotes to fetch
      responseQueue.push(
        await client.scroll({
          scrollId: body._scroll_id,
          scroll: '1m'
        })
      )
    }
    return completeData;
  }

  let filter=(array)=>
  {

     return array.sort(() => Math.random() - 0.5).splice(0,3000); 
  }


  module.exports={getalldata,filter}