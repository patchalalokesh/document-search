
const mongoose = require('mongoose');
require('dotenv').config();
const fileuploadmodel = require('../models/fileuploadmodel');
const db =  process.env.DB;
mongoose.connect(db);
let getuploadfiles=async( page=1,limit=10)=>{
    

        console.log('entered');
        // execute query with page and limit values
         let files = await fileuploadmodel.find({}).select("-file")
          .limit(limit * 1)
          .skip((page - 1) * limit)
          .exec().then(); 
        //  console.log(files);
          
           // get total documents in the Posts collection 
         return  await fileuploadmodel.countDocuments().then(response=>{
             return {
                files,
                totalPages:Math.ceil(response / limit),
                currentPage:page
                 
             }
         });
        }
module.exports={getuploadfiles};