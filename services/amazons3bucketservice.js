const AWS = require('aws-sdk');
require('dotenv').config();
const id= process.env.S3BUCKETID;
const secret= process.env.AMAZON_SECRET;


const s3 = new AWS.S3({
            accessKeyId: id,
            secretAccessKey: secret
        });
// const ID = '';
// const SECRET = '';

// The name of the bucket that you have created


// let createBucket = (bucketName)=>{
//     console.log(id,secret);
//     
//     const params = {
//         Bucket:bucketName,
//         CreateBucketConfiguration: {
//             // Set your region here
//             LocationConstraint: "eu-west-1"
//         }
//     };
//     return s3.createBucket(params, (response,error)=>{
//         console.log(response);
//         if(error)
//         {
//             console.log(error)
//         }
//         // return new promise()
//     })
// }

let uploadFile = (buffer,filename) => {
    // Read content from the file
    // const fileContent = fs.readFileSync(buffer);

    // Setting up S3 upload parameters
    const params = {
        Bucket: process.env.BUCKET_NAME,
        Key:filename, // File name you want to save as in S3
        Body: buffer
    };

    // Uploading files to the bucket
    return new Promise((resolve,reject)=>{
        console.log('entered');
         s3.upload(params,(error,response)=>{
            if(response)
            {
                console.log("response")
                console.log(response,"53");
                resolve(response);
            }
            if(error)
            {
                console.log(error);
                reject(error);
            }
        })
    })
    
}

let deleteaFile = (filename)=>{
    var params = {
        Bucket: process.env.BUCKET_NAME,
        Key: filename
        };
       return s3.deleteObject(params).then(data=>{
            console.log(data,"72");
        }).catch(error=>{
            console.log()
        })
            // , function (err, data) {
            //     if (data) {
            //     console.log("File deleted successfully");
            //     }
            //     else {
            //     console.log("Check if you have sufficient permissions : "+err);
            //     }
            //     });
}


module.exports={ uploadFile, deleteaFile}