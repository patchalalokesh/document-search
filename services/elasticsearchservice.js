// var elasticsearch = require('elasticsearch');
require('dotenv').config();
const { Client } = require('@elastic/elasticsearch');

// var client = new elasticsearch.Client({
//     host:`https:${process.env.ELASTIC_SEARCH_CLOUDID}//:${process.env.ELASTIC_SEARCH_PASSWORD}@[server]:[port]/`,
//    // host: 'localhost:9200',
//     log: 'trace',
//     apiVersion: '7.2', // use the same version of your Elasticsearch instance
// });

const client = new Client({
    cloud: {
      id: process.env.ELASTIC_SEARCH_CLOUDID
    },
    auth: {
      username:process.env.ELASTIC_SEARCH_USERNAME,
      password: process.env.ELASTIC_SEARCH_PASSWORD
    }
  })

var airportdata = require('../experimentaldata/airportdata');


let createDocument = async (id, body, filename, filesize, fileformat, createddate, lat, lng) => {
    return await client.create({
        index: 'documentsearch',//index of the document
        id,
        body: {
            body,
            filename,
            filesize,
            fileformat,
            createddate,
            lat,
            lng

            //   title: 'Test 1',
            //   tags: ['y', 'z'],
            //   published: true,
            //   published_at: '2013-01-01',
            //   counter: 1
        }
    });
}

let deleteDocument = async (id) => {
    return await client.delete({
        index: 'documentsearch',

        id: id
    });

}

//search based on geo location
let locationSearch = (lat, lon, distance) => {
    let body = {
        "size":"10000",
        "query": {
            "bool": {
                "must": {
                    "match_all": {}
                },
                "filter": {
                    "geo_distance": {
                        "distance": `${distance}km`,
                        "location": {
                            lat,
                            lon,
                        }
                    }
                }
            }
        }
    }
    return search("latlngsearch", body)
}

//search based on country code
let SearchusingcountryCode = (countryCode) => {
    let body = {
        "from": "0",
        "size": "10000",
        "query": {
            "match": {
                "iso_country": countryCode
            }
        }
    }
    return SearchCountryCode("latlngsearch",body);
}

//multi query search

const search = function search(index, body) {
    return client.search({ index: index, body: body });
};

const SearchCountryCode= function search(index,body)
{
    return client.search({index:index,body:body});
}




const searchForQuery = (query) => {
    let body = {
        size: 20,
        from: 0,
        query: {

            multi_match: {
                query,
                fields: ['filename', 'body']
            }
        }

    };

    return search('documentsearch', body);
    // .then(results => {
    //   console.log(`found ${results.hits.total} items in ${results.took}ms`);
    //   console.log(`returned article titles:`);
    //   results.hits.hits.forEach(
    //     (hit, index) => console.log(
    //       `\t${body.from + ++index} - ${hit._source.title}`
    //     )
    //   )
    // })
    // .catch(console.error);
};

//inserting bulk data
//   latlngsearch

const bulkIndex = function bulkIndex(index, data) {
    let bulkBody = [];

    data.forEach(item => {
        let coordinates = item.coordinates.split(',');
        let lon = coordinates[0].trim();
        let lat = coordinates[1].trim();
        //  console.log(lat,lng);
        bulkBody.push({
            index: {
                _index: index

            }
        });

        bulkBody.push({
            ident: item.ident,
            name: item.name,
            type: item.type,
            elevation_ft: item.elevation_ft,
            continent: item.continent,
            iso_country: item.iso_country,
            iso_region: item.iso_region,
            municipality: item.municipality,
            gps_code: item.gps_code,
            iata_code: item.iata_code,
            local_code: item.local_code,
            location: {
                lon,
                lat
            }
        }
        );
        //   console.log(bulkBody);
    });



    return client.bulk({ body: bulkBody })
        .then(response => {
            console.log('here');
            let errorCount = 0;
            console.log(response);
            response.items.forEach(item => {
                if (item.index && item.index.error) {
                    console.log(++errorCount, item.index.error);
                }
            });
            console.log(
                `Successfully indexed ${data.length - errorCount}
         out of ${data.length} items`
            );
        })
        .catch(console.err);
};

const uploadbulkdata = () => {
    // const articlesRaw = fs.readFileSync('data.json');
    let datatoUpload = airportdata.airportData;
    return bulkIndex('latlngsearch', datatoUpload);
};




module.exports = {
    createDocument, deleteDocument, searchForQuery, uploadbulkdata, locationSearch, SearchusingcountryCode
}