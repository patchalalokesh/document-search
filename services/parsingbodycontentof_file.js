const StreamZip = require('node-stream-zip');
var PdfReader = require("pdfreader").PdfReader;
const WordExtractor = require("word-extractor"); 

exports.extract = function (buffer) {
    const extractor = new WordExtractor();
    const extracted = extractor.extract(buffer);

    return extracted.then(function(doc) { return doc.getBody() });
};

exports.getFileExtension = function (filename) {
if (filename.length == 0)
return "";
var dot = filename.lastIndexOf(".");
if (dot == -1)
return "";
var extension = filename.substr(dot, filename.length);
return extension;
};
exports.readPDFFile = function (pdfFilePath, pdfBuffer) {
return new Promise(
function (resolve, reject) {
var content = ''
new PdfReader().parseBuffer(pdfBuffer, function (err, item) {
if (err) {
reject(err);
} else if (!item) {
reject(err);
} else if (item.text) {
content = item.text;
resolve(content.toString())
}
});
}
)
};
